## [1.0.10](https://gitlab.com/andreassolberg/badgekit/compare/v1.0.9...v1.0.10) (2024-06-30)


### Bug Fixes

* Add property size to addIcon ([2cf849c](https://gitlab.com/andreassolberg/badgekit/commit/2cf849cf35740cfd7498f214acd4113830d7ca65))
* Add unique instance id for each badge object, making sure the ids do not conflice ([9305fbb](https://gitlab.com/andreassolberg/badgekit/commit/9305fbb9da1527d29d174f9c0e0c09b5b5c346b6))
* Improvements to addText. Adding fontfamily and more. ([2f58896](https://gitlab.com/andreassolberg/badgekit/commit/2f5889641ef2b74ce446373131194e0fa06f61f7))
* Improvements to addTextArc() adding fontfamily and more ([eb021b8](https://gitlab.com/andreassolberg/badgekit/commit/eb021b817ad7a8e0e7c68219f46a3e0bf6e5fc0f))

## [1.0.9](https://gitlab.com/andreassolberg/badgekit/compare/v1.0.8...v1.0.9) (2024-06-30)


### Bug Fixes

* Testing rollup instead of parcel ([a166308](https://gitlab.com/andreassolberg/badgekit/commit/a166308d6cf29ea9dc67e385f7f125c1797d2a71))

## [1.0.8](https://gitlab.com/andreassolberg/badgekit/compare/v1.0.7...v1.0.8) (2024-06-30)


### Bug Fixes

* Trying local file ([714f585](https://gitlab.com/andreassolberg/badgekit/commit/714f5856a1bd0562d9450a15d1f8bbce58e17094))

## [1.0.7](https://gitlab.com/andreassolberg/badgekit/compare/v1.0.6...v1.0.7) (2024-06-30)


### Bug Fixes

* Set module property ([7e41052](https://gitlab.com/andreassolberg/badgekit/commit/7e41052024342631460af0e7b2416553aba628d7))

## [1.0.6](https://gitlab.com/andreassolberg/badgekit/compare/v1.0.5...v1.0.6) (2024-06-29)


### Bug Fixes

* add type = module ([a1a9263](https://gitlab.com/andreassolberg/badgekit/commit/a1a92631421f314e26ead08bc24a0785d503094a))

## [1.0.5](https://gitlab.com/andreassolberg/badgekit/compare/v1.0.4...v1.0.5) (2024-06-29)


### Bug Fixes

* point to not built file ([0354852](https://gitlab.com/andreassolberg/badgekit/commit/0354852ee32f43565242ac0b94be1f509a75fdd1))

## [1.0.4](https://gitlab.com/andreassolberg/badgekit/compare/v1.0.3...v1.0.4) (2024-06-29)


### Bug Fixes

* add node version ([a8194c8](https://gitlab.com/andreassolberg/badgekit/commit/a8194c889c878ce415313badaf6439779f02f7ff))
* update ci spec ([92728fa](https://gitlab.com/andreassolberg/badgekit/commit/92728fa3482dc3569568fb8c0fb38fcf2e897201))

## [1.0.3](https://gitlab.com/andreassolberg/badgekit/compare/v1.0.2...v1.0.3) (2024-06-29)


### Bug Fixes

* update artifact path ([7d23665](https://gitlab.com/andreassolberg/badgekit/commit/7d23665e6daa34279f5b984a00fe95ebafcca2ea))

## [1.0.2](https://gitlab.com/andreassolberg/badgekit/compare/v1.0.1...v1.0.2) (2024-06-29)


### Bug Fixes

* add dist folder as artifact ([b39e875](https://gitlab.com/andreassolberg/badgekit/commit/b39e875f7805fd3bc30f952631bc3192c1434e72))

## [1.0.1](https://gitlab.com/andreassolberg/badgekit/compare/v1.0.0...v1.0.1) (2024-06-29)


### Bug Fixes

* add to readme. test release ([ce84482](https://gitlab.com/andreassolberg/badgekit/commit/ce84482ba3df75b9dffc9a55b56d362b1845a082))

# 1.0.0 (2024-06-29)


### Bug Fixes

* Fix entrypoint in package.json ([78548f2](https://gitlab.com/andreassolberg/badgekit/commit/78548f28dec6f545d5599a40eb170d52611ad492))
