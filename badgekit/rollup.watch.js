import replace from "@rollup/plugin-replace";

export default {
  input: "Badge.js",
  output: {
    file: "../src/badgekit/badgekit.js",
    format: "es",
    name: "badgekit",
  },
  external: ["d3"],

  plugins: [
    replace({
      delimiters: ["", ""],
      values: { "from 'd3'": "from 'npm:d3'" },
    }),
  ],
};
