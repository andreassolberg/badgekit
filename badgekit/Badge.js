import * as d3 from "d3";

const findCircleCoordinates = (r, pos) => {
  // Calculating the x-coordinate
  // cosine(angle) gives the x-coordinate of the point on the circle
  const angleInRadians = pos * Math.PI * 2 - Math.PI / 2;
  var x = r * Math.cos(angleInRadians);

  // Calculating the y-coordinate
  // sine(angle) gives the y-coordinate of the point on the circle
  var y = r * Math.sin(angleInRadians);

  return { x: x, y: y };
};

const ringCreator = (inner, outer) => {
  return (r, pos) => findCircleCoordinates(inner + r * (outer - inner), pos);
};

export class Badge {
  constructor() {
    this.instance = Math.random().toString(36).substring(7);
    this.height = 300;
    this.width = 300;
    this.margin = 5;
    this.inner = this.height - 2 * this.margin;
    this.id = 0;

    this.rScale = d3
      .scaleLinear()
      .domain([0, 1])
      .range([0, this.inner / 2]);

    this.xScale = d3
      .scaleLinear()
      .domain([0, 1])
      .range([0, 2 * Math.PI]);
    this.xScaleArc = (x) => x * 2 * Math.PI;

    this.svg = d3
      .create("svg")
      .attr("viewport", `0 0 ${this.width} ${this.height}`)
      .attr("width", this.width)
      .attr("height", this.height);
    this.root = this.svg
      .append("g")
      .attr(
        "transform",
        `translate(${this.margin + this.inner / 2}, ${
          this.margin + this.inner / 2
        })`
      );

    this.layers = [];
    this.layerMap = new Map();
    this.themes = new Map();
  }

  addTheme(cfg) {
    this.themes.set(cfg.id, cfg);
  }
  prepareConfig(type, cfg, defaultCfg) {
    let theme = {};
    defaultCfg.id = "l" + this.instance + "-" + this.id++;
    if (cfg.theme) {
      theme = this.themes.get(cfg.theme);
    }
    const mergedCfg = { ...defaultCfg, ...theme, ...cfg, type };
    return mergedCfg;
  }

  addRing(cfg) {
    this.layers.push(
      this.prepareConfig("ring", cfg, {
        r: 0.5,
        stroke: "black",
        strokeWidth: 1,
        fill: "none",
        opacity: 1,
        strokeDasharray: "none",
      })
    );
  }

  addCircle(cfg) {
    this.layers.push(
      this.prepareConfig("circle", cfg, {
        r: 0.5,
        stroke: "black",
        strokeWidth: 1,
        fill: "none",
        opacity: 1,
      })
    );
  }

  addArc(cfg) {
    if (cfg.r) {
      cfg.r0 = cfg.r;
      cfg.r1 = cfg.r;
    }
    this.layers.push(
      this.prepareConfig("arc", cfg, {
        r0: 0.5,
        r1: 0.8,
        x0: 0,
        x1: 0.4,
        fill: "#cc88ee",
        stroke: "none",
        strokeWidth: 1,
        opacity: 1,
      })
    );
  }

  addRipple(cfg) {
    this.layers.push(
      this.prepareConfig("ripple", cfg, {
        r: 0.5,
        stroke: "black",
        strokeWidth: 1,
        fill: "yellow",
        opacity: 1,
        r0: 0.8,
        r1: 0.9,
      })
    );
  }

  addText(cfg) {
    this.layers.push(
      this.prepareConfig("text", cfg, {
        text: "Hello",
        fontSize: 16,
        fontFamily: "sans-serif",
        fill: "black",
        opacity: 1,
        dx: 0,
        dy: 0,
      })
    );
  }

  addTextArc(cfg) {
    this.layers.push(
      this.prepareConfig("textarc", cfg, {
        r: 0.5,
        text: "Hello",
        fontSize: 16,
        fontFamily: "sans-serif",
        fill: "black",
        opacity: 1,
      })
    );
  }

  addIcon(cfg) {
    if (cfg.size) {
      cfg.height = cfg.size;
      cfg.width = cfg.size;
    }
    this.layers.push(
      this.prepareConfig("icon", cfg, { width: 0.2, height: 0.2, dx: 0, dy: 0 })
    );
  }

  show() {
    this.layers.forEach((layer) => {
      if (layer.type === "ring") {
        this.root
          .append("circle")
          .attr("cx", 0)
          .attr("cy", 0)
          .attr("r", this.rScale(layer.r))
          .attr("fill", layer.fill)
          .attr("stroke", layer.stroke)
          .attr("stroke-width", layer.strokeWidth)
          .attr("stroke-dasharray", layer.strokeDasharray)
          .attr("opacity", layer.opacity);
      } else if (layer.type === "icon") {
        this.root
          .append("image")
          .attr("href", layer.href)
          .attr("x", -this.rScale(layer.width) / 2)
          .attr("y", -this.rScale(layer.height) / 2)
          .attr("width", this.rScale(layer.width))
          .attr("height", this.rScale(layer.height));
      } else if (layer.type === "text") {
        this.root
          .append("text")
          .attr("x", this.rScale(layer.dx))
          .attr("y", this.rScale(layer.dy))
          .attr("font-size", layer.fontSize)
          .attr("text-anchor", "middle")
          .attr("dominant-baseline", "middle")
          .attr("font-family", layer.fontFamily)
          .attr("fill", layer.fill)
          .attr("opacity", layer.opacity)
          .text(layer.label);
      } else if (layer.type === "arc") {
        console.log("Adding arc", layer);
        console.log("startAngle", this.xScale(layer.x0));
        console.log("endAngle", this.xScale(layer.x1));
        const arc = d3
          .arc()
          .innerRadius(this.rScale(layer.r0))
          .outerRadius(this.rScale(layer.r1))
          .startAngle(this.xScale(layer.x0))
          .endAngle(this.xScale(layer.x1));
        if (layer.rounded) arc.cornerRadius(300);
        let arcid = `arc${layer.id}`;
        this.root
          .append("path")
          .attr("id", arcid)
          .attr("d", arc)
          .attr("fill", layer.fill)
          .attr("stroke", layer.stroke)
          .attr("stroke-width", layer.strokeWidth)
          .attr("opacity", layer.opacity);
      } else if (layer.type === "textarc") {
        const arc1 = d3
          .arc()
          .innerRadius(this.rScale(layer.r))
          .outerRadius(this.rScale(layer.r))
          .startAngle(this.xScale(layer.x - 0.5))
          .endAngle(this.xScale(layer.x + 0.5 - 0.01));
        let arcid = `arc${layer.id}`;
        this.root.append("path").attr("id", arcid).attr("d", arc1);
        this.root
          .append("text")
          .append("textPath")
          .attr("xlink:href", "#" + arcid)
          .attr("startOffset", layer.x < 0.75 && layer.x > 0.25 ? "75%" : "25%")
          .style("text-anchor", "middle")
          .style("dominant-baseline", "middle")
          .attr("font-size", layer.fontSize)
          .attr("text-anchor", "middle")
          .attr("dominant-baseline", "middle")
          .attr("font-family", layer.fontFamily)
          .attr("fill", layer.fill)
          .attr("opacity", layer.opacity)
          .text(layer.label);
      } else if (layer.type === "circle") {
        this.root
          .append("circle")
          .attr("cx", 0)
          .attr("cy", 0)
          .attr("r", this.rScale(layer.r))
          .attr("fill", layer.fill)
          .attr("stroke", layer.stroke)
          .attr("stroke-width", layer.strokeWidth)
          .attr("opacity", layer.opacity);
      } else if (layer.type === "ripple") {
        let c = d3.curveLinearClosed;
        if (layer.curve === "linear") c = d3.curveLinearClosed;
        if (layer.curve === "basis") c = d3.curveBasisClosed;
        if (layer.curve === "catmull") c = d3.curveCatmullRomClosed;

        const shape = d3.radialLine().curve(c);
        let el = d3
          .range(0, layer.no)
          .map((de) => [
            (de * Math.PI * 2) / layer.no,
            de % 2 === 0 ? this.rScale(layer.r0) : this.rScale(layer.r1),
          ]);
        console.log("EL", el);
        const path = shape(el);
        this.root
          .append("path")
          .attr("d", path)
          .attr("stroke", layer.stroke ? layer.stroke : "none")
          .attr("stroke-width", layer.strokeWidth ? layer.strokeWidth : 1)
          .attr("fill", layer.fill ? layer.fill : "none");
      }
    });

    return this.svg.node();
  }
}
