export default {
  input: "Badge.js",
  output: {
    file: "dist/bundle.js",
    format: "amd",
    name: "badgekit",
  },
};
