---
toc: true
---

```js
import { Badge } from "./badgekit/badgekit.js";
```

# Reference

## addRing()

## addArc()

The arc is a part of a circle. The arc is defined by the radius `r`, the start angle `x0` and the end angle `x1`.

Here are all the properties of the arc:

- `r` - the radius of the arc as a line
- `r0` - the inner radius of the arc
- `r1` - the outer radius of the arc
- `x0` - the start angle of the arc
- `x1` - the end angle of the arc
- `fill` - the fill color of the arc
- `stroke` - the stroke color of the arc
- `strokeWidth` - the width of the stroke
- `rounded` - if true, the arc will be rounded

If you specify the `r` property (instead of `r0` and `r1`), the arc will be a line.

```js echo
let arc1 = new Badge();
arc1.addArc({
  r: 1,
  x0: -0.2,
  x1: 0.1,
  stroke: "#aaccdd",
  strokeWidth: 2,
});
display(arc1.show());
```

If you instead of `r` specify `r0` and `r1`, the arc will be an arc area.

```js echo
let arc2 = new Badge();
arc2.addArc({
  r0: 0.6,
  r1: 0.8,
  x0: -0.2,
  x1: 0.4,
  fill: "#aaccdd",
  rounded: true,
});
display(arc2.show());
```

A new example with other properties.

```js echo
let arc3 = new Badge();
arc3.addArc({
  r0: 0.2,
  r1: 0.8,
  x0: -0.2,
  x1: 0.2,
  fill: "#ee99aa",
  stroke: "black",
});
display(arc3.show());
```

```js echo
let arc4 = new Badge();
arc4.addArc({
  r0: 0.4,
  r1: 0.8,
  x0: 0.2,
  x1: 0.3,
  fill: "#ff9999",
  stroke: "black",
  strokeWidth: 4,
});
display(arc4.show());
```

The default direction of the arc is clock-wise. If the x1 is a negative number, the arc will be counter-clockwise.

```js echo
let arc5 = new Badge();
arc5.addArc({
  r0: 0.4,
  r1: 0.8,
  x0: 0.2,
  x1: -0.7,
  fill: "#ff9999",
  stroke: "black",
  strokeWidth: 4,
});
display(arc5.show());
```

## addRipple()

```js echo
let r1 = new Badge();
r1.addRipple({
  stroke: "black",
  fill: "#abcbdb",
  no: 36,
  r0: 0.5,
  r1: 0.6,
  curve: "catmull",
});
display(r1.show());
```

## addText()

```js echo
let t1 = new Badge();
t1.addRing({ r: 0.5 });
t1.addText({
  label: "2024",
  fontSize: 54,
  fontFamily: "Arial",
});
t1.addText({
  label: "January",
  fontFamily: "monospace",
  fill: "#485293",
  fontSize: 24,
  dy: 0.25,
});
display(t1.show());
```

## addTextArc()

Add text following an arc.

```js echo
let at1 = new Badge();
at1.addTextArc({
  label: "Best of the year",
  fontSize: 18,
  r: 0.8,
  x: 0.5,
});
display(at1.show());
```

```js echo
let at2 = new Badge();
at2.addTextArc({
  label: "Best of the year",
  fontSize: 28,
  r: 0.9,
  x: 0,
});
display(at2.show());
```

## addIcon()

```js echo
let i1 = new Badge();
i1.addRing({ r: 0.5 });
i1.addIcon({
  href: "https://upload.wikimedia.org/wikipedia/commons/2/27/ClassicPortable.svg",
  size: 0.8,
});
display(i1.show());
```
