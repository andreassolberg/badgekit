---
toc: false
---

```js
import { Badge } from "./badgekit/badgekit.js";
```

# Badge examples

```js echo
let b1 = new Badge();
b1.addTheme({ id: "1", stroke: "green" });
b1.addRipple({
  stroke: "black",
  fill: "#ff8833",
  no: 100,
  r0: 0.7,
  r1: 0.72,
  curve: "linear",
});
b1.addRipple({
  stroke: "black",
  fill: "#ccddcc",
  no: 20,
  r0: 0.5,
  r1: 0.8,
  curve: "catmull",
});
b1.addRing({ stroke: "green", r: 0.35 });
b1.addRing({ theme: "1", r: 0.4, opacity: 0.3, strokeDasharray: "8,2" });
b1.addRing({
  stroke: "green",
  fill: "#44cc88",
  r: 0.2,
});
b1.addRing({ stroke: "#449944", fill: "#77aaff", r: 0.12, opacity: 0.3 });
b1.addText({ label: "Sikt", fontSize: 18 });
display(b1.show());
```

Example 2

```js echo
let b1 = new Badge();
b1.addTheme({ id: "1", stroke: "green" });
b1.addRipple({
  stroke: "black",
  fill: "#f0e8e8",
  no: 18,
  r0: 0.5,
  r1: 0.72,
  curve: "basis",
});
b1.addRipple({
  stroke: "black",
  fill: "#f8e9ea",
  no: 36,
  r0: 0.4,
  r1: 0.6,
  curve: "basis",
});
b1.addIcon({
  href: "https://upload.wikimedia.org/wikipedia/commons/b/b1/Cross-Country_Skiing_-_The_Noun_Project.svg",
  width: 0.3,
  height: 0.3,
});
b1.addTextArc({
  label: "Raskest på langrenn",
  fontSize: 18,
  r: 0.34,
  x: 0,
});
b1.addTextArc({
  label: "Vinter 23/24",
  fontSize: 10,
  r: 0.21,
  x: 0,
});
b1.addTextArc({
  label: "30 km fri",
  fontSize: 16,
  r: 0.3,
  x: 0.5,
});
// b1.addRing({ stroke: "black", opacity: 0.5, r: 0.23, strokeWidth: 0.4 });
display(b1.show());
```
