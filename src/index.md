---
toc: false
---

# BadgeKit

Importing the library:

```js echo run=false
import { Badge } from "../badgekit/Badge.js";
```

```js
import { Badge } from "./badgekit/badgekit.js";
```

Create a badge:

```js echo
let b = new Badge();
```

Add the layers

```js echo
b.addRing({ stroke: "#6699cc", r: 0.9 });
```

And then display it:

```js echo
display(b.show());
```
