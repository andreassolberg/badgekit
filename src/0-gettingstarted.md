---
toc: false
---

```js
import { Badge } from "./badgekit/badgekit.js";
```

# Getting started

Importing the library:

```js echo run=false
import { Badge } from "badgekit";
```

Create a badge:

```js echo
let b = new Badge();
```

Add the layers

```js echo
b.addRing({ stroke: "#6699cc", r: 0.9 });
```

And then display it:

```js echo
display(b.show());
```
